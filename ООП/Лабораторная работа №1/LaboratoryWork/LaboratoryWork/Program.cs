﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaboratoryWork1
{
    class Program
    {
        public const double Sixteen = 16;

        public static Mass MassFromPounds (double Ounces)
        {
            double Pounds = Ounces * Sixteen;
            Mass temp = new Mass (Pounds);
            return (temp);
        }

        static void Main (string[] args)
        {
            double MassPart;
            Mass FirstMassInPounds = new Mass();
            Mass SecondMassInPounds = new Mass();
            Mass ResultOfSum = new Mass();
            Mass ResultOfSub = new Mass();
            Mass FirstMassInOunces = new Mass();

            Console.Write (" Enter the weight in pounds for the first object: ");
            MassPart = Convert.ToDouble (Console.ReadLine());
            FirstMassInPounds.MassInPounds = MassPart;
            Console.WriteLine();

            Console.Write (" Enter the weight in pounds for the second object: ");
            MassPart = Convert.ToDouble (Console.ReadLine());
            SecondMassInPounds.MassInPounds = MassPart;
            Console.WriteLine();

            Console.WriteLine (" The mass of the first object: ");
            FirstMassInPounds.Print ();
            Console.WriteLine();

            Console.WriteLine (" The mass of the second object: ");
            SecondMassInPounds.Print ();
            Console.WriteLine();

            Console.WriteLine (" Result of sum: ");
            ResultOfSum = FirstMassInPounds + SecondMassInPounds;
            ResultOfSum.Print();
            Console.WriteLine();

            Console.WriteLine (" Result of sub: ");
            ResultOfSub = FirstMassInPounds - SecondMassInPounds;
            ResultOfSub.Print();
            Console.WriteLine();

            Console.Write (" Enter the weight in ounces for the first object: ");
            MassPart = Convert.ToDouble(Console.ReadLine());
            FirstMassInOunces.MassInPounds = MassPart;
            Console.WriteLine();
            Mass Ounces = MassFromPounds (MassPart);
            Ounces.Print();
            Console.WriteLine();


            Console.ReadKey();
        }
    }
}
