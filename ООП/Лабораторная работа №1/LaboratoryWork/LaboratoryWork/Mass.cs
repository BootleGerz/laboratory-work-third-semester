﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaboratoryWork1
{
    class Mass
    {
        public const double Sixteen = 16;
        public const double FourHunderedFiftyThree = 453.6;

        protected double MassValueInPounds;

        #region Properties
        public double MassInGrams
        {
            get
            {
                return MassValueInPounds * FourHunderedFiftyThree;
            }
        }

        public double MassInPounds
        {
            get
            {
                return MassValueInPounds;
            }
            set
            {
                MassValueInPounds = value;
            }
        }

        public double MassInOunces
        {
            get
            {
                return MassValueInPounds / Sixteen;
            }
        }
        #endregion

        #region Constructors
        public Mass()
        {
            this.MassInPounds = 0.0;
        }
        public Mass (double Pounds)
        {
            this.MassInPounds = Pounds;
        }
        #endregion

        #region Methods and Function
        public static Mass Sum (Mass FirstMass, Mass SecondMass)
        {
            Mass Result = new Mass();
            Result.MassInPounds = FirstMass.MassInPounds + SecondMass.MassInPounds;
            return Result;
        }

        public static Mass operator + (Mass FirsMass, Mass SecondMass)
        {
            return Mass.Sum (FirsMass, SecondMass);
        }

        public static Mass Substraction (Mass FirstMass, Mass SecondMass)
        {
            Mass Result = new Mass();
            Result.MassInPounds = FirstMass.MassInPounds - SecondMass.MassInPounds;
            return Result;
        }

        public static Mass operator - (Mass FirsMass, Mass SecondMass)
        {
            return Mass.Substraction (FirsMass, SecondMass);
        }

        public void Print()
        {
            Console.WriteLine (" Pounds = {0} ", this.MassInPounds);
            double Ounces = (this.MassInPounds / Sixteen);
            Console.WriteLine (" Ounces= " + Ounces);
        }
        #endregion
    }
}
