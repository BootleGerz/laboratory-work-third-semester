﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Matrix
    {
        private int _dimension;
        private int[,] _content;

        public Matrix(int N)
        {
            _content = new int [N, N];
            _dimension = N;
        }

        public void MultiplyOnScalar(int number)
        {
            for (int i = 0; i < _dimension; i++)
            {
                for (int j = 0; j < _dimension; j++)
                {
                    _content[i, j] = _content [i, j] * number;
                }
            }
        }


        public void InputMatrix()
        {
            Random rnd = new Random();
            for (int i = 0; i < _dimension; i++)
            {
                for (int j = 0; j < _dimension; j++)
                {
                    _content[i, j] = rnd.Next(-20, 20);
                }
            }
        }


        public void OutputMatrix()
        {
            for (int i = 0; i < _dimension; i++)
            {
                for (int j = 0; j < _dimension; j++)
                {
                    Console.Write(String.Format("{0, -11:0}", _content[i, j]));
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }



    }
}
