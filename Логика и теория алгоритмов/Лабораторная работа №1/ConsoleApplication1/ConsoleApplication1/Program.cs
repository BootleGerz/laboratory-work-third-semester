﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            int number;
            Console.Write("Enter N ");
            N = int.Parse (Console.ReadLine());
            Matrix a = new Matrix(N);
            a.InputMatrix();
            a.OutputMatrix();

            Console.Write("Enter number ");
            number= int.Parse (Console.ReadLine());
            a.MultiplyOnScalar(number);
            a.OutputMatrix();

            Console.ReadKey();



        }
        
    }
}
